package com.abc.workshop.cicd.drugstore.service;

import java.util.List;

import com.abc.workshop.cicd.drugstore.dto.CustomerDTO;

/**
 * Service interface for performing customer-related operations.
 */
public interface CustomerService {
    /**
     * Returns the list of all customer objects.
     *
     * @return list of all customer objects
     */
    List<CustomerDTO> getAllCustomers();

    /**
     * Returns the customer object for the specified customer ID if found.
     *
     * @param customerId The customer ID
     * @return The customer object
     */
    CustomerDTO getCustomer(Long customerId);

    /**
     * Saves customer info.
     *
     * @param request The customer information
     * @return the updated customer object
     */
    CustomerDTO saveCustomer(CustomerDTO request);

    /**
     * Deletes an existing customer.
     *
     * @param customerId The ID of the customer to delete
     */
    void deleteCustomer(Long customerId);
}

package com.abc.common.advice;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Component
public class LoggingAdvice {
    static final Logger logger = LoggerFactory.getLogger(LoggingAdvice.class);

    public LoggingAdvice() {}

    @Pointcut("execution (public * *(..))")
    public void anyPublicMethod() {}

    @Pointcut("within (com.abc.*.*)")
    public void inServiceLayer() {}

    @Pointcut("anyPublicMethod() && inServiceLayer()")
    public void anyPublicServiceMethod() {}

    @Around("anyPublicServiceMethod()")
    public Object profile(ProceedingJoinPoint joinPoint) throws Throwable {
        String methodSignature = this.formatMethodSignature(joinPoint);
        StopWatch stopWatch = new StopWatch();
        logger.debug(String.format("==== method: %s ====", new Object[] {methodSignature}));
        Object[] var4 = joinPoint.getArgs();
        int var5 = var4.length;

        for (int var6 = 0; var6 < var5; ++var6) {
            Object arg = var4[var6];
            logger.debug(String.format("\t[%s]", new Object[] {arg}));
        }

        Object var11;
        try {
            if (!methodSignature.contains("AuthenticationFilter")) {
                logger.info(String.format("=> %s", new Object[] {methodSignature}));
                if (!stopWatch.isRunning()) {
                    stopWatch.start();
                }
            }

            var11 = joinPoint.proceed();
        } finally {
            if (stopWatch.isRunning()) {
                stopWatch.stop();
                logger.info(String.format("<= %s - elapsed time: %ss", new Object[] {methodSignature, Double.valueOf(stopWatch.getTotalTimeSeconds())}));
            }

        }

        return var11;
    }

    private String formatMethodSignature(JoinPoint joinPoint) {
        joinPoint.getSignature().getName();
        return joinPoint.getSignature().toString();
    }
}

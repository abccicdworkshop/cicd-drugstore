package com.abc.workshop.cicd.drugstore.controllers;

import com.abc.workshop.cicd.drugstore.exception.NotEnoughProductsInStockException;
import com.abc.workshop.cicd.drugstore.service.ProductService;
import com.abc.workshop.cicd.drugstore.service.ShoppingCartService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private ProductService productService;

    @GetMapping("/shoppingCart")
    public ModelAndView shoppingCart() {
        ModelAndView modelAndView = new ModelAndView("shoppingCart");
        return addProperties(modelAndView);
    }

    @GetMapping("/shoppingCart/addProduct/{productId}")
    public ModelAndView addProductToCart(@PathVariable("productId") Long productId) {
        shoppingCartService.addProduct(productService.getProduct(productId));
        return shoppingCart();
    }

    @GetMapping("/shoppingCart/removeProduct/{productId}")
    public ModelAndView removeProductFromCart(@PathVariable("productId") Long productId) {
        shoppingCartService.removeProduct(productService.getProduct(productId));
        return shoppingCart();
    }

    @GetMapping("/product/{productId}")
    public ModelAndView viewProduct(@PathVariable("productId") Long productId) {
        ModelAndView modelAndView = new ModelAndView("product");
        modelAndView.addObject("product", productService.getProduct(productId));
        return addProperties(modelAndView);
    }

    private ModelAndView addProperties(ModelAndView modelAndView) {
        modelAndView.addObject("productMap", shoppingCartService.getProductsInCart());
        modelAndView.addObject("products", shoppingCartService.getProductsInCart().keySet());
        modelAndView.addObject("total", shoppingCartService.getTotal().toString());
        return modelAndView;
    }

    @GetMapping("/shoppingCart/checkout")
    public ModelAndView checkout() {
        try {
            shoppingCartService.checkout();
        } catch (NotEnoughProductsInStockException e) {
            return shoppingCart().addObject("outOfStockMessage", e.getMessage());
        }
        return shoppingCart();
    }
}

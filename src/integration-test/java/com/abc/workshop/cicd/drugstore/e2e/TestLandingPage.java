package com.abc.workshop.cicd.drugstore.e2e;

import java.net.MalformedURLException;

import com.abc.workshop.cicd.drugstore.e2e.page.CartPage;
import com.abc.workshop.cicd.drugstore.e2e.page.LandingPage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestLandingPage extends BaseSeleniumTest {

    LandingPage landingPage;
    CartPage cartPage;

    @Before
    public void setup() throws MalformedURLException {
        super.setup();
        landingPage = new LandingPage(driver);
        cartPage = new CartPage(driver);
    }

    @After
    public void teardown() {
        super.teardown();
    }

    @Test
    public void testOnLandingPage() {
        //Verify home page
        Assert.assertTrue(landingPage.isOnLandingPage());
    }


    @Test
    public void whenItemAddedToShoppingCartGoToShoppingCartPage() {
        Assert.assertTrue(landingPage.isOnLandingPage());
        Assert.assertTrue(landingPage.getFirstProductSection().isAddButtonDisplayed());

        landingPage.getFirstProductSection().clickAddButton();

        Assert.assertTrue(cartPage.isOnCartPage());

        Assert.assertEquals("Total: 6.01", cartPage.getTotalAmountText());
    }

}

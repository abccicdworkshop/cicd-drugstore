package com.abc.workshop.cicd.drugstore.dao;

import java.util.List;

import com.abc.workshop.cicd.drugstore.model.Address;

public interface AddressDAO {
    /**
     * Returns the list of all address objects
     * @return The list of all address objects
     */
    List<Address> getAllAddresses();

    /**
     * Returns the address object for the specified address ID if found.
     * @param addressId The address ID
     * @return The address object
     */
    Address getAddress(Long addressId);

    /**
     * Save address info.
     * @param request The address object to be saved.
     * @return The updated address object
     */
    Address saveAddress(Address request);

    /**
     * Deletes an existing address.
     * @param addressId The address ID
     */
    void deleteAddress(Long addressId);

    /**
     * Returns the list of all address objects for a specific Customer.
     * @param customerId The customer ID
     * @return The list of all address objects for that customer.
     */
    List<Address> getAllAddressesForCustomer(Long customerId);
}

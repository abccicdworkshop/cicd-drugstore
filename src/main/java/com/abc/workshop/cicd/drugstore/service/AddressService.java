package com.abc.workshop.cicd.drugstore.service;

import java.util.List;

import com.abc.workshop.cicd.drugstore.dto.AddressDTO;

/**
 * Service interface for performing address-related operations.
 */
public interface AddressService {
    /**
     * Returns the list of all address objects
     * @return The list of all address objects
     */
    List<AddressDTO> getAllAddresses();

    /**
     * Returns the address object for the specified address ID if found.
     * @param addressId The address ID
     * @return The address object
     */
    AddressDTO getAddress(Long addressId);

    /**
     * Save address info.
     * @param request The address object to be saved
     * @return The updated address object
     */
    AddressDTO saveAddress(AddressDTO request);

    /**
     * Deletes an existing address.
     * @param addressId The address ID
     */
    void deleteAddress(Long addressId);

    /**
     * Returns the list of all address objects for a Customer.
     * @param customerId The customer ID
     * @return The list of all address objects.
     */
    List<AddressDTO> getAllAddressesForCustomer(Long customerId);
}

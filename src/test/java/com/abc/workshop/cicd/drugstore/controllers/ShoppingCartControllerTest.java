package com.abc.workshop.cicd.drugstore.controllers;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.HashMap;

import com.abc.workshop.cicd.drugstore.dto.ProductDTO;
import com.abc.workshop.cicd.drugstore.service.ProductService;
import com.abc.workshop.cicd.drugstore.service.ShoppingCartService;
import com.abc.workshop.cicd.drugstore.utils.TestDataHelper;

import org.hamcrest.core.StringContains;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ShoppingCartControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private ProductService productService;


    @MockBean
    private ShoppingCartService shoppingCartService;


    @Test
    public void verifiesShoppingCartPageHasNoItems() throws Exception {

        Mockito.when(this.shoppingCartService.getProductsInCart()).thenReturn(new HashMap<ProductDTO, Integer>());
        Mockito.when(this.shoppingCartService.getTotal()).thenReturn(new BigDecimal(0));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/shoppingCart"))
                .andExpect(MockMvcResultMatchers.content().contentType(new MediaType(MediaType.TEXT_HTML, Charset.forName("UTF-8"))))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Home")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Total: 0")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Checkout")));
    }

    @Test
    public void verifiesShoppingCartPageHasItems() throws Exception {

        Mockito.when(this.shoppingCartService.getProductsInCart()).thenReturn(new HashMap<ProductDTO, Integer>() {
            {
                put(TestDataHelper.getProduct(1), 1);
                put(TestDataHelper.getProduct(3), 2);
            }
        });
        Mockito.when(this.shoppingCartService.getTotal()).thenReturn(new BigDecimal("77.77"));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/shoppingCart"))
                .andExpect(MockMvcResultMatchers.content().contentType(new MediaType(MediaType.TEXT_HTML, Charset.forName("UTF-8"))))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Home")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test product 1")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Price: $11.11")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Quantity in cart: 1")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test product 3")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Price: $33.33")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Quantity in cart: 2")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Total: 77.77")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Checkout")));
    }

    @Test
    public void testAddProductToCart() throws Exception {

        Mockito.when(this.productService.getProduct(1L)).thenReturn(TestDataHelper.getProduct(1));
        Mockito.when(this.shoppingCartService.getProductsInCart()).thenReturn(new HashMap<ProductDTO, Integer>() {
            {
                put(TestDataHelper.getProduct(1), 1);
            }
        });
        Mockito.when(this.shoppingCartService.getTotal()).thenReturn(new BigDecimal("11.11"));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/shoppingCart/addProduct/1"))
                .andExpect(MockMvcResultMatchers.content().contentType(new MediaType(MediaType.TEXT_HTML, Charset.forName("UTF-8"))))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Quantity in cart: 1")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Add to cart")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Remove")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Total: 11.11")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Checkout")));
    }

    @Test
    public void testRemoveProductToCart() throws Exception {

        Mockito.when(this.productService.getProduct(1L)).thenReturn(TestDataHelper.getProduct(1));
        Mockito.when(this.shoppingCartService.getProductsInCart()).thenReturn(new HashMap<ProductDTO, Integer>());
        Mockito.when(this.shoppingCartService.getTotal()).thenReturn(new BigDecimal("0"));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/shoppingCart/removeProduct/1"))
                .andExpect(MockMvcResultMatchers.content().contentType(new MediaType(MediaType.TEXT_HTML, Charset.forName("UTF-8"))))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Home")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Total: 0")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Checkout")));
    }

    @Test
    public void testViewProduct() throws Exception {

        Mockito.when(this.productService.getProduct(1L)).thenReturn(TestDataHelper.getProduct(1));
        Mockito.when(this.shoppingCartService.getProductsInCart()).thenReturn(new HashMap<ProductDTO, Integer>());
        Mockito.when(this.shoppingCartService.getTotal()).thenReturn(new BigDecimal("0"));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/product/1"))
                .andExpect(MockMvcResultMatchers.content().contentType(new MediaType(MediaType.TEXT_HTML, Charset.forName("UTF-8"))))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Home")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test product 1")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Add to cart")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Price: $11.11")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("In Stock: 1")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Add to cart")));
    }

}

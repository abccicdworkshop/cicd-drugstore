package com.abc.workshop.cicd.drugstore.dto;

import java.time.LocalDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductCategoryDTO {
    private Long productCategoryId;
    private String name;
    private LocalDateTime createdDateTime;
    private String createdBy;
    private LocalDateTime modifiedDateTime;
    private String modifiedBy;

    public Long getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    @JsonProperty("createdDateTime")
    public String getCreatedDateTimeAsString() {
        return createdDateTime.toString();
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getModifiedDateTime() {
        return modifiedDateTime;
    }

    @JsonProperty("modifiedDateTime")
    public String getModifiedDateTimeAsString() {
        return modifiedDateTime.toString();
    }

    public void setModifiedDateTime(LocalDateTime modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ProductCategoryDTO that = (ProductCategoryDTO) o;
        return Objects.equals(getProductCategoryId(), that.getProductCategoryId()) && Objects.equals(getName(), that.getName())
                && Objects.equals(getCreatedDateTime(), that.getCreatedDateTime()) && Objects.equals(getCreatedBy(), that.getCreatedBy())
                && Objects.equals(getModifiedDateTime(), that.getModifiedDateTime()) && Objects.equals(getModifiedBy(), that.getModifiedBy());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductCategoryId(), getName(), getCreatedDateTime(), getCreatedBy(), getModifiedDateTime(), getModifiedBy());
    }
}

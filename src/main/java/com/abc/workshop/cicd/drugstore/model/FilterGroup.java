package com.abc.workshop.cicd.drugstore.model;

import java.util.List;

public class FilterGroup {
    private Integer groupOp;
    private List<FilterCondition> conditions;

    public Integer getGroupOp() {
        return groupOp;
    }

    public void setGroupOp(Integer groupOp) {
        this.groupOp = groupOp;
    }

    public List<FilterCondition> getConditions() {
        return conditions;
    }

    public void setConditions(List<FilterCondition> conditions) {
        this.conditions = conditions;
    }
}

package com.abc.common.exception;


public class BadRequestException extends RuntimeException {
    private static final long serialVersionUID = -1888192473417873058L;

    public BadRequestException() {}

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(Throwable cause) {
        super(cause);
    }
}
